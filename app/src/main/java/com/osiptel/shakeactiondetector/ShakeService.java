package com.osiptel.shakeactiondetector;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

public class ShakeService extends Service implements ShakeDetection.Listener {
    private static final String TAG = ShakeService.class.getSimpleName();

    private ShakeDetection sd;
    private boolean isScreenOff;

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mScreenStateReceiver, screenStateFilter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private BroadcastReceiver mScreenStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isScreenOff = intent.getBooleanExtra("screen_state",true);
            if(isScreenOff){
                Log.e("Service", "SCREEN OFF");
            }
            if(!isScreenOff){
                Log.e("Service", "SCREEN ON");
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Service", "startCommand");
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sd = new ShakeDetection(this);
        sd.start(sensorManager);
        isScreenOff = intent.getBooleanExtra("screen_state" , true);
        Log.e("isScrennOff", String.valueOf(isScreenOff));
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mScreenStateReceiver);
    }

    @Override
    public void hearShake(boolean isShake) {
        if(isScreenOff){
            PowerManager pm = (PowerManager) getApplicationContext().getSystemService(getApplicationContext().POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), TAG);
            wakeLock.acquire();
            //and release again
            wakeLock.release();

            Log.e("Service", "hearShake first");
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
            isScreenOff = false;
        }else{
            Log.e("Service", "hearShake stop");
            this.stopSelf();
        }

    }
}
